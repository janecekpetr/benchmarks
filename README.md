```sh
mvn clean verify

# And then one of:
java -jar target/benchmarks.jar ArraycopyBenchmark
java -jar target/benchmarks.jar ArraycopyExistingBenchmark
java -jar target/benchmarks.jar CompressionBenchmark
java -jar target/benchmarks.jar CollectionLiteralBenchmark
java -jar target/benchmarks.jar DecompressionBenchmark
java -jar target/benchmarks.jar DurationFormatBenchmark
java -jar target/benchmarks.jar FloatSerializationBenchmark
java -jar target/benchmarks.jar LocalDateParsingBenchmark
java -jar target/benchmarks.jar LockBenchmark
java -jar target/benchmarks.jar LoopFissionBenchmark
java -jar target/benchmarks.jar MultiLambdaBenchmark
java -jar target/benchmarks.jar ProtobufTimestampSerializationBenchmark
java -jar target/benchmarks.jar RandomGeneratorBenchmark
java -jar target/benchmarks.jar StreamCollectorBenchmark
java -jar target/benchmarks.jar StringConcatBenchmark
java -jar target/benchmarks.jar StringIndexOfBenchmark
java -jar target/benchmarks.jar StringSplitBenchmark

# optionally with profiling
-prof async:output=flamegraph
-prof gc

# or machine-friendly output
-rf csv
-rf json
-rf latex
-rf scsv
```