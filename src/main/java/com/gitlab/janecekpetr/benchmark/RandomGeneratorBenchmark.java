package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.concurrent.TimeUnit;
import java.util.random.RandomGenerator;

/**
 * <pre>
 * Benchmark          (randomType)  Mode  Cnt     Score   Error  Units
 * nextInt            SecureRandom  avgt    5   864,246 ± 1,430  ns/op
 * nextInt                  Random  avgt    5     9,025 ± 0,016  ns/op
 * nextInt        SplittableRandom  avgt    5     1,901 ± 0,006  ns/op
 * nextInt       ThreadLocalRandom  avgt    5     2,153 ± 0,004  ns/op
 * nextInt         L32X64MixRandom  avgt    5     3,014 ± 0,076  ns/op
 * nextInt        L64X128MixRandom  avgt    5     3,126 ± 0,005  ns/op
 * nextInt        L64X256MixRandom  avgt    5     3,422 ± 0,008  ns/op
 * nextInt       L64X1024MixRandom  avgt    5     4,059 ± 0,019  ns/op
 * nextInt       L128X128MixRandom  avgt    5     7,309 ± 0,016  ns/op
 * nextInt       L128X256MixRandom  avgt    5     6,879 ± 0,004  ns/op
 * nextInt      L128X1024MixRandom  avgt    5     6,849 ± 0,010  ns/op
 * nextInt   L64X128StarStarRandom  avgt    5     2,806 ± 0,005  ns/op
 * nextInt      Xoshiro256PlusPlus  avgt    5     2,740 ± 0,006  ns/op
 * nextInt    Xoroshiro128PlusPlus  avgt    5     2,618 ± 0,001  ns/op
 *
 * nextLong           SecureRandom  avgt    5  1697,445 ± 2,668  ns/op
 * nextLong                 Random  avgt    5    18,056 ± 0,009  ns/op
 * nextLong       SplittableRandom  avgt    5     2,077 ± 0,003  ns/op
 * nextLong      ThreadLocalRandom  avgt    5     2,142 ± 0,002  ns/op
 * nextLong        L32X64MixRandom  avgt    5     4,881 ± 0,008  ns/op
 * nextLong       L64X128MixRandom  avgt    5     2,912 ± 0,001  ns/op
 * nextLong       L64X256MixRandom  avgt    5     3,196 ± 0,003  ns/op
 * nextLong      L64X1024MixRandom  avgt    5     3,999 ± 0,026  ns/op
 * nextLong      L128X128MixRandom  avgt    5     4,410 ± 0,020  ns/op
 * nextLong      L128X256MixRandom  avgt    5     7,706 ± 0,009  ns/op
 * nextLong     L128X1024MixRandom  avgt    5     6,261 ± 0,016  ns/op
 * nextLong  L64X128StarStarRandom  avgt    5     2,727 ± 0,003  ns/op
 * nextLong     Xoshiro256PlusPlus  avgt    5     2,750 ± 0,143  ns/op
 * nextLong   Xoroshiro128PlusPlus  avgt    5     2,467 ± 0,007  ns/op
 * </pre>
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class RandomGeneratorBenchmark {

	@Param
	private RandomType randomType;

	private RandomGenerator random;
	private int upperBound;

	@Setup
	public void prepare() {
		random = randomType.create();
		upperBound = 32768;
	}

//	@Benchmark
	public int nextInt() {
		return random.nextInt();
	}

	@Benchmark
	public int nextIntBounded() {
		return random.nextInt(upperBound);
	}

//	@Benchmark
	public long nextLong() {
		return random.nextLong();
	}

	public enum RandomType {
		SecureRandom,
		Random,
		SplittableRandom,
		ThreadLocalRandom {
			@Override
			public RandomGenerator create() {
				return java.util.concurrent.ThreadLocalRandom.current();
			}
		},

		L32X64MixRandom,	// default in at least Java 17-21
		L64X128MixRandom,
		L64X256MixRandom,
		L64X1024MixRandom,
		L128X128MixRandom,
		L128X256MixRandom,
		L128X1024MixRandom,

		L64X128StarStarRandom,

		Xoshiro256PlusPlus,
		Xoroshiro128PlusPlus;

		public RandomGenerator create() {
			return RandomGenerator.of(name());
		}
	}
}