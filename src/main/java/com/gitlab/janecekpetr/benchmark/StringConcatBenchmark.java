package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.time.Clock;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class StringConcatBenchmark {

	private String type;
    private long time;

	@Setup
	public void prepare() {
		type = "this is a type I think";
        time = Clock.systemUTC().millis();
	}

    @Benchmark
    public String stringBuilder() {
        return new StringBuilder().append(type).append(":").append(time).toString();
    }

    @Benchmark
    public String stringBuilderChar() {
        return new StringBuilder().append(type).append(':').append(time).toString();
    }

    @Benchmark
    public String stringBuilderSized() {
        return new StringBuilder(type.length() + 1 + 13).append(type).append(":").append(time).toString();
    }

    @Benchmark
    public String stringBuilderCharSized() {
        return new StringBuilder(type.length() + 1 + 13).append(type).append(':').append(time).toString();
    }

    @Benchmark
    public String stringBuilderInitialized() {
        return new StringBuilder(type).append(":").append(time).toString();
    }

    @Benchmark
    public String stringBuilderInitializedChar() {
        return new StringBuilder(type).append(':').append(time).toString();
    }

    @Benchmark
    public String plus() {
        return type + ":" + time;
    }

    @Benchmark
    public String plusChar() {
        return type + ':' + time;
    }

    @Benchmark
    public String concat() {
        return type.concat(":").concat(String.valueOf(time));
    }

    @Benchmark
    public String concatChar() {
        return type.concat(String.valueOf(':')).concat(String.valueOf(time));
    }

}
