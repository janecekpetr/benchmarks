package com.gitlab.janecekpetr.benchmark;

import com.google.protobuf.CodedOutputStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.io.IOException;
import java.io.OutputStream;
import java.time.InstantSource;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class ProtobufTimestampSerializationBenchmark {

    private long timestamp;
    private CodedOutputStream codedOutputStream;

    @Setup
    public void prepare(Blackhole blackhole) {
        timestamp = InstantSource.system().millis();
        codedOutputStream = CodedOutputStream.newInstance(new BlackholeOutputStream(blackhole));
    }

    @Benchmark
    public void int64() throws IOException {
        codedOutputStream.writeInt64NoTag(timestamp);
    }

    @Benchmark
    public void uint64() throws IOException {
        codedOutputStream.writeUInt64NoTag(timestamp);
    }

    @Benchmark
    public void fixed64() throws IOException {
        codedOutputStream.writeFixed64NoTag(timestamp);
    }

    @Benchmark
    public void sfixed64() throws IOException {
        codedOutputStream.writeSFixed64NoTag(timestamp);
    }

    private static class BlackholeOutputStream extends OutputStream {

        private final Blackhole blackhole;

        public BlackholeOutputStream(Blackhole blackhole) {
            this.blackhole = blackhole;
        }

        @Override
        public void write(int data) throws IOException {
            blackhole.consume(data);
        }

        @Override
        public void write(byte[] bytes, int offset, int length) throws IOException {
            blackhole.consume(bytes);
        }
    }

}
