package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;
import java.util.random.RandomGenerator;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class LocalDateParsingBenchmark {

    private static final DateTimeFormatter BASIC_ISO_DATE = DateTimeFormatter.BASIC_ISO_DATE;
    private static final DateTimeFormatter BASIC_ISO_DATE_NO_OFFSET =
            new DateTimeFormatterBuilder()
                .appendValue(ChronoField.YEAR, 4)
                .appendValue(ChronoField.MONTH_OF_YEAR, 2)
                .appendValue(ChronoField.DAY_OF_MONTH, 2)
                .toFormatter();
    private static final DateTimeFormatter BASIC_DATE_FROM_PATTERN = DateTimeFormatter.ofPattern("yyyyMMdd");

    private String date;

    @Setup
    public void prepare() {
        RandomGenerator random = new SplittableRandom(42);
        int year = random.nextInt(2000, 2100);
        int month = random.nextInt(10, 13);
        int day = random.nextInt(10, 29);
        date = String.valueOf(year) + month + day;
    }

    @Benchmark
    public LocalDate basicIsoDateParser() {
        return LocalDate.parse(date, BASIC_ISO_DATE);
    }

    @Benchmark
    public LocalDate basicIsoDateParserWithNoZoneInfo() {
        return LocalDate.parse(date, BASIC_ISO_DATE_NO_OFFSET);
    }

    @Benchmark
    public LocalDate basicIsoDateParserFromPattern() {
        return LocalDate.parse(date, BASIC_DATE_FROM_PATTERN);
    }

    @Benchmark
    public LocalDate basicIsoDateParserCustom() {
        int dateInt = Integer.parseInt(date);

        int dayOfMonth = dateInt % 100;
        dateInt /= 100;

        int month = dateInt % 100;
        dateInt /= 100;

        int year = dateInt;

        return LocalDate.of(year, month, dayOfMonth);
    }

}
