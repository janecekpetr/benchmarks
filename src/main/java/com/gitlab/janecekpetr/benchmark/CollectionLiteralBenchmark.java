package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 3, time = 3)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class CollectionLiteralBenchmark {

	private static final Set<String> CONSTANT = Set.of("str1", "str2", "str3");

	private Set<String> field;
	private String needle;

    @Setup
    public void prepare() {
    	field = Set.of("str1", "str2", "str3");
    	needle = "noodle";
    }

	@Benchmark
	public boolean createdEveryTime() {
		return Set.of("str1", "str2", "str3").contains(needle);
    }

	@Benchmark
	public boolean field() {
		return field.contains(needle);
    }

	@Benchmark
	public boolean privateStaticFinalField() {
		return CONSTANT.contains(needle);
    }

	@Benchmark
	public boolean switchExpression() {
		return switch (needle) {
			case "str1", "str2", "str3" -> true;
			default -> false;
		};
	}

}
