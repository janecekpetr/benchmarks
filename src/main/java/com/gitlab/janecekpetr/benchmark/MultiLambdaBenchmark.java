package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class MultiLambdaBenchmark {

	private Callable<Integer> task1;
	private Callable<Integer> task2;
	private Callable<Integer> task3;
	private Callable<Integer> task4;

	@Setup
	public void prepare() {
		task1 = () -> 0;
		task2 = () -> 1;
		task3 = () -> 2;
		task4 = () -> 3;
	}

	@Benchmark
	public void singleLambda1(Blackhole bh) throws Exception {
		bh.consume(task1.call());
	}

	@Benchmark
	public void singleLambda2(Blackhole bh) throws Exception {
		bh.consume(task2.call());
	}

	@Benchmark
	public void singleLambda3(Blackhole bh) throws Exception {
		bh.consume(task3.call());
	}

	@Benchmark
	public void singleLambda4(Blackhole bh) throws Exception {
		bh.consume(task4.call());
	}

}
