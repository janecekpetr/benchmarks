package com.gitlab.janecekpetr.benchmark;

import com.gitlab.janecekpetr.util.stream.SizedCollectors;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@State(Scope.Benchmark)
public class StreamCollectorBenchmark {

	@Param({"0", "1", "1000", "1000000"})
	private int size;
	@Param
	private SourceType sourceType;

	private List<String> list;

	@Setup
	public void prepare() {
		list = sourceType.createList(Collections.nCopies(size, "aha"));
	}

	@Benchmark
	public List<String> baselineListCopyOf() {
		return List.copyOf(list);
	}

	@Benchmark
	public List<String> baselineArrayListCopyConstructor() {
		return new ArrayList<>(list);
	}

	@Benchmark
	public List<String> baselineArrayListAddAll() {
		List<String> newList = new ArrayList<>();
		newList.addAll(list);
		return newList;
	}

	@Benchmark
	public List<String> toArray() {
		return Arrays.asList(list.stream().toArray(String[]::new));
	}

	@Benchmark
	public List<String> toList() {
		return list.stream().toList();
	}

	@Benchmark
	public List<String> collectToList() {
		return list.stream().collect(Collectors.toList());
	}

	@Benchmark
	public List<String> collectToSizedList() {
		return SizedCollectors.collectToSizedList(list.stream());
	}

	@Benchmark
	public List<String> collectToSizedListSequentially() {
		return SizedCollectors.collectToSizedListSequentially(list.stream());
	}

	/** Stream source, actually sometimes makes an interesting difference. */
	public enum SourceType {
		ARRAY_LIST {
			@Override
			public <T> List<T> createList(List<T> listToTransform) {
				return new ArrayList<>(listToTransform);
			}
		},
		LIST_OF {
			@Override
			public <T> List<T> createList(List<T> listToTransform) {
				return List.copyOf(listToTransform);
			}
		},
		TO_LIST {
			@Override
			public <T> List<T> createList(List<T> listToTransform) {
				return listToTransform.stream().toList();
			}
		};

		public abstract <T> List<T> createList(List<T> listToTransform);
	}

}