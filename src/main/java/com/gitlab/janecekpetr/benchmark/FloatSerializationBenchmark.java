package com.gitlab.janecekpetr.benchmark;

import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.unsafe.UnsafeOutput;
import com.gitlab.janecekpetr.util.io.FixedSizeByteArrayOutputStream;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.common.io.LittleEndianDataOutputStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import sun.misc.Unsafe;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.foreign.MemorySegment;
import java.lang.foreign.ValueLayout;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;
import java.util.random.RandomGenerator;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class FloatSerializationBenchmark {

    private static final VarHandle FLOATS_AS_BYTES_LITTLE_ENDIAN
    		= MethodHandles.byteArrayViewVarHandle(float[].class, ByteOrder.LITTLE_ENDIAN).withInvokeExactBehavior();
    private static final VarHandle FLOATS_AS_BYTES_BIG_ENDIAN
    		= MethodHandles.byteArrayViewVarHandle(float[].class, ByteOrder.BIG_ENDIAN).withInvokeExactBehavior();

    @Param({/*"32", "128", */"512"/*, "2048", "4096"*/})
    private int size;

    private float[] floats;
    private int bytesLength;

    @Setup
    public void setup() {
        floats = new float[size];
        RandomGenerator random = new SplittableRandom(42);
        for (int i = 0; i < floats.length; i++) {
            floats[i] = random.nextFloat();
        }

        bytesLength = size * Float.BYTES;
    }

    @Benchmark
    public byte[] beManualUnpacking() {
        byte[] bytes = new byte[bytesLength];
        for (int floatsIndex = 0, bytesIndex = 0; floatsIndex < floats.length; floatsIndex++, bytesIndex += Float.BYTES) {
            int value = Float.floatToIntBits(floats[floatsIndex]);
            bytes[bytesIndex    ] = (byte)(value >> 24);
            bytes[bytesIndex + 1] = (byte)(value >> 16);
            bytes[bytesIndex + 2] = (byte)(value >> 8);
            bytes[bytesIndex + 3] = (byte)value;
        }
        return bytes;
    }

    @Benchmark
    public byte[] leManualUnpacking() {
        byte[] bytes = new byte[bytesLength];
        for (int floatsIndex = 0, bytesIndex = 0; floatsIndex < floats.length; floatsIndex++, bytesIndex += Float.BYTES) {
            int value = Float.floatToIntBits(floats[floatsIndex]);
            bytes[bytesIndex    ] = (byte)value;
            bytes[bytesIndex + 1] = (byte)(value >> 8);
            bytes[bytesIndex + 2] = (byte)(value >> 16);
            bytes[bytesIndex + 3] = (byte)(value >> 24);
        }
        return bytes;
    }

    @Benchmark
    public byte[] beDataOutputStream() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytesLength);
		try (DataOutputStream outputStream = new DataOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
		}
        return byteArrayOutputStream.toByteArray();
    }

    @Benchmark
    public byte[] beDataOutputStreamFixedSize() throws IOException {
        FixedSizeByteArrayOutputStream byteArrayOutputStream = new FixedSizeByteArrayOutputStream(bytesLength);
		try (DataOutputStream outputStream = new DataOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
		}
		// Intentionally does not use toByteArray() to avoid copying. The internal buffer was precisely sized.
        return byteArrayOutputStream.buffer();
    }

    @Benchmark
    public byte[] beGuavaDataOutputStream() {
        ByteArrayDataOutput dataOutput = ByteStreams.newDataOutput(bytesLength);
        for (float element : floats) {
            dataOutput.writeFloat(element);
        }
        return dataOutput.toByteArray();
    }

    @Benchmark
    public byte[] leDataOutputStream() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytesLength);
        try (LittleEndianDataOutputStream outputStream = new LittleEndianDataOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Benchmark
    public byte[] leDataOutputStreamFixedSize() throws IOException {
    	FixedSizeByteArrayOutputStream byteArrayOutputStream = new FixedSizeByteArrayOutputStream(bytesLength);
        try (LittleEndianDataOutputStream outputStream = new LittleEndianDataOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
        }
        // Intentionally does not use toByteArray() to avoid copying. The internal buffer was precisely sized.
        return byteArrayOutputStream.buffer();
    }

    /** Much slower than DataOutputStream before JDK 17. */
    @Benchmark
    public byte[] beObjectOutputStream() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytesLength);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Benchmark
    public byte[] beObjectOutputStreamFixedSize() throws IOException {
    	FixedSizeByteArrayOutputStream byteArrayOutputStream = new FixedSizeByteArrayOutputStream(bytesLength);
        try (ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream)) {
	        for (float element : floats) {
	            outputStream.writeFloat(element);
	        }
        }
        // Intentionally does not use toByteArray() to avoid copying. The internal buffer was precisely sized.
        return byteArrayOutputStream.buffer();
    }

    @Benchmark
    public byte[] leByteBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength).order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.asFloatBuffer().put(floats);
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] leByteBufferWrap() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength).order(ByteOrder.LITTLE_ENDIAN);
        // This was twice as fast as the variant with no wrapping until Java 17:
        byteBuffer.asFloatBuffer().put(FloatBuffer.wrap(floats));
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] leByteBufferNoBatching() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength).order(ByteOrder.LITTLE_ENDIAN);
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        for (float f : floats) {
            floatBuffer.put(f);
        }
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] beByteBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength);
        byteBuffer.asFloatBuffer().put(floats);
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] beByteBufferWrap() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength);
        // This was half as fast as the variant with no wrapping until Java 17:
        byteBuffer.asFloatBuffer().put(FloatBuffer.wrap(floats));
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] beByteBufferNoBatching() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytesLength);
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        for (float f : floats) {
            floatBuffer.put(f);
        }
        return byteBuffer.array();
    }

    @Benchmark
    public byte[] beKryo() {
        try (Output output = new Output(bytesLength)) {
            output.writeFloats(floats, 0, floats.length);
            // Intentionally does not use getBytes() to avoid copying. The internal buffer was precisely sized.
            return output.getBuffer();
        }
    }

    @Benchmark
    @Fork(jvmArgsAppend = "--add-opens=java.base/sun.nio.ch=ALL-UNNAMED")
    public byte[] leKryoUnsafe() {
        try (UnsafeOutput output = new UnsafeOutput(bytesLength)) {
            output.writeFloats(floats, 0, floats.length);
            // Intentionally does not use getBytes() to avoid copying. The internal buffer was precisely sized.
            return output.getBuffer();
        }
    }

    @Benchmark
    public byte[] leUnsafeCopyMemory() {
        int bytesLength = floats.length * Float.BYTES;
        byte[] bytes = new byte[bytesLength];
        UnsafeHolder.unsafe.copyMemory(
        		floats, Unsafe.ARRAY_FLOAT_BASE_OFFSET,
        		bytes, Unsafe.ARRAY_BYTE_BASE_OFFSET,
        		bytesLength);
        return bytes;
    }

    @Benchmark
    public byte[] leVarHandle() {
        byte[] bytes = new byte[bytesLength];
        for (int i = 0; i < floats.length; i++) {
        	FLOATS_AS_BYTES_LITTLE_ENDIAN.set(bytes, i * Float.BYTES, floats[i]);
        }
        return bytes;
    }

    @Benchmark
    public byte[] beVarHandle() {
        byte[] bytes = new byte[bytesLength];
        for (int i = 0; i < floats.length; i++) {
            FLOATS_AS_BYTES_BIG_ENDIAN.set(bytes, i * Float.BYTES, floats[i]);
        }
        return bytes;
    }

    @Benchmark
    @Fork(jvmArgsAppend = "--add-modules=jdk.incubator.foreign")
    public byte[] leMemorySegment() {
    	MemorySegment segment = MemorySegment.ofArray(floats);
        return segment.toArray(ValueLayout.JAVA_BYTE);
    }

    private static class UnsafeHolder {

	    private static final Unsafe unsafe;
	    static {
	    	try {
	    		final Field f = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
	    		f.setAccessible(true);
	    		unsafe = (Unsafe)f.get(null);
	    	} catch (NoSuchFieldException | IllegalAccessException e) {
	    		throw new IllegalStateException("Failed to initialize UnsafeUtil", e);
	    	}
	    }

	}

}
