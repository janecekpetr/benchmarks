package com.gitlab.janecekpetr.benchmark;

import com.google.common.base.Splitter;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Benchmark                                                    (string)  Mode  Cnt    Score     Error  Units
 * baseline                                             Petr,Anna,Stepan  avgt    5   27.805 ±   0.444  ns/op
 * guavaSplitterCreatedEverytime                        Petr,Anna,Stepan  avgt    5   77.065 ±   0.924  ns/op
 * guavaSplitter                                        Petr,Anna,Stepan  avgt    5   98.906 ±   1.235  ns/op
 * guavaSplitterIterable                                Petr,Anna,Stepan  avgt    5   98.528 ±   5.703  ns/op
 * guavaSplitterIterableConsume                         Petr,Anna,Stepan  avgt    5   87.617 ±   9.377  ns/op
 * guavaSplitterIterableIterate                         Petr,Anna,Stepan  avgt    5   85.613 ±   1.476  ns/op
 * manualIndexOfChar                                    Petr,Anna,Stepan  avgt    5   53.486 ±   1.825  ns/op
 * manualIndexOfString                                  Petr,Anna,Stepan  avgt    5   55.776 ±   6.761  ns/op
 * manualSinglePass                                     Petr,Anna,Stepan  avgt    5  101.603 ±  16.316  ns/op
 * patternSplit                                         Petr,Anna,Stepan  avgt    5  126.313 ±   3.701  ns/op
 * patternSplitStreamToList                             Petr,Anna,Stepan  avgt    5  180.181 ±   3.427  ns/op
 * stringSplit                                          Petr,Anna,Stepan  avgt    5   74.951 ±   2.290  ns/op
 * stringSplitCorrect                                   Petr,Anna,Stepan  avgt    5   74.893 ±   3.867  ns/op
 * stringTokenizer                                      Petr,Anna,Stepan  avgt    5   66.190 ±   1.461  ns/op
 * stringTokenizerIterator                              Petr,Anna,Stepan  avgt    5   82.873 ±   7.719  ns/op
 * stringTokenizerIteratorConsume                       Petr,Anna,Stepan  avgt    5   71.076 ±   3.533  ns/op
 *
 * baseline                        nula,jedna,dva,...,osmnact,devatenact  avgt    5  225.427 ±   1.281  ns/op
 * guavaSplitterCreatedEverytime   nula,jedna,dva,...,osmnact,devatenact  avgt    5  462.968 ±  38.419  ns/op
 * guavaSplitter                   nula,jedna,dva,...,osmnact,devatenact  avgt    5  456.642 ±  23.222  ns/op
 * guavaSplitterIterable           nula,jedna,dva,...,osmnact,devatenact  avgt    5  489.772 ± 118.245  ns/op
 * guavaSplitterIterableConsume    nula,jedna,dva,...,osmnact,devatenact  avgt    5  387.137 ±  20.746  ns/op
 * guavaSplitterIterableIterate    nula,jedna,dva,...,osmnact,devatenact  avgt    5  523.487 ±  23.652  ns/op
 * manualIndexOfChar               nula,jedna,dva,...,osmnact,devatenact  avgt    5  345.396 ±  22.439  ns/op
 * manualIndexOfString             nula,jedna,dva,...,osmnact,devatenact  avgt    5  336.771 ±  18.008  ns/op
 * manualSinglePass                nula,jedna,dva,...,osmnact,devatenact  avgt    5  711.134 ±  70.236  ns/op
 * patternSplit                    nula,jedna,dva,...,osmnact,devatenact  avgt    5  617.498 ±  14.003  ns/op
 * patternSplitStreamToList        nula,jedna,dva,...,osmnact,devatenact  avgt    5  925.714 ±  23.937  ns/op
 * stringSplit                     nula,jedna,dva,...,osmnact,devatenact  avgt    5  389.357 ±  36.562  ns/op
 * stringSplitCorrect              nula,jedna,dva,...,osmnact,devatenact  avgt    5  408.145 ±  57.083  ns/op
 * stringTokenizer                 nula,jedna,dva,...,osmnact,devatenact  avgt    5  455.255 ±  10.578  ns/op
 * stringTokenizerIterator         nula,jedna,dva,...,osmnact,devatenact  avgt    5  518.340 ±  41.389  ns/op
 * stringTokenizerIteratorConsume  nula,jedna,dva,...,osmnact,devatenact  avgt    5  430.306 ±  22.943  ns/op
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class StringSplitBenchmark {

    private static final Pattern PATTERN = Pattern.compile(",");
    private static final Splitter SPLITTER = Splitter.on(',');

    @Param({
        "Petr,Anna,Stepan",
        "nula,jedna,dva,tri,ctyri,pet,sest,sedm,osm,devet,deset,jedenact,dvanact,trinact,ctrnact,patnact,sestnact,sedmnact,osmnact,devatenact"
    })
    private String string;

    @Benchmark
    public String[] baseline() {
        if (string.length() == 16) {
            String[] result = new String[3];
            result[0] = string.substring(0, 4);
            result[1] = string.substring(5, 9);
            result[2] = string.substring(10);
            return result;
        }

        String[] result = new String[20];
        result[0] = string.substring(0, 4);
        result[1] = string.substring(5, 10);
        result[2] = string.substring(11, 14);
        result[3] = string.substring(15, 18);
        result[4] = string.substring(19, 24);
        result[5] = string.substring(25, 28);
        result[6] = string.substring(29, 33);
        result[7] = string.substring(34, 38);
        result[8] = string.substring(39, 42);
        result[9] = string.substring(43, 48);
        result[10] = string.substring(49, 54);
        result[11] = string.substring(55, 63);
        result[12] = string.substring(64, 71);
        result[13] = string.substring(72, 79);
        result[14] = string.substring(80, 88);
        result[15] = string.substring(89, 95);
        result[16] = string.substring(96, 104);
        result[17] = string.substring(105, 113);
        result[18] = string.substring(114, 121);
        result[19] = string.substring(122, 132);
        return result;
    }

    @Benchmark
    public List<String> manualIndexOfChar() {
        List<String> result = new ArrayList<>();

        int previous = 0;
        int next = 0;
        while ((next = string.indexOf(',', previous)) != -1) {
            String token = string.substring(previous, next);
            result.add(token);
            previous = next + 1;
        }

        String lastToken = string.substring(previous);
        result.add(lastToken);

        return result;
    }

    @Benchmark
    public List<String> manualIndexOfString() {
        List<String> result = new ArrayList<>();

        int previous = 0;
        int next = 0;
        while ((next = string.indexOf(",", previous)) != -1) {
            String token = string.substring(previous, next);
            result.add(token);
            previous = next + 1;
        }

        String lastToken = string.substring(previous);
        result.add(lastToken);

        return result;
    }

    @Benchmark
    public List<String> manualSinglePass() {
        List<String> result = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        for (int index = 0; index < string.length(); index++) {
            char c = string.charAt(index);
            if (c != ',') {
                builder.append(c);
            } else {
                String token = builder.toString();
                result.add(token);
                builder.setLength(0);
            }
        }
        String lastToken = builder.toString();
        result.add(lastToken);
        return result;
    }

    @Benchmark
    public String[] stringSplit() {
        return string.split(",");
    }

    @Benchmark
    public String[] stringSplitCorrect() {
        return string.split(",", -1);
    }

    @Benchmark
    public String[] patternSplit() {
        return PATTERN.split(string);
    }

    @Benchmark
    public List<String> patternSplitStreamToList() {
        return PATTERN.splitAsStream(string)
            .toList();
    }

    @Benchmark
    public List<String> stringTokenizerToList() {
        List<String> result = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(string, ",");
        while (tokenizer.hasMoreTokens()) {
            result.add(tokenizer.nextToken());
        }
        return result;
    }

    @Benchmark
    public List<String> stringTokenizerAsIteratorToList() {
        List<String> result = new ArrayList<>();
        new StringTokenizer(string, ",")
            .asIterator()
            .forEachRemaining(token -> result.add((String)token));
        return result;
    }

    @Benchmark
    public void stringTokenizerAsIteratorConsume(Blackhole bh) {
        new StringTokenizer(string, ",")
            .asIterator()
            .forEachRemaining(bh::consume);
    }

    @Benchmark
    public List<String> guavaSplitterToListCreatedEverytime() {
        return Splitter.on(',')
            .splitToList(string);
    }

    @Benchmark
    public List<String> guavaSplitterToList() {
        return SPLITTER.splitToList(string);
    }

    @Benchmark
    public List<String> guavaSplitterIterableAddedToArrayList() {
        List<String> result = new ArrayList<>();
        SPLITTER.split(string)
            .forEach(result::add);
        return result;
    }

    @Benchmark
    public void guavaSplitterIterableForEach(Blackhole bh) {
        SPLITTER.split(string)
            .forEach(bh::consume);
    }

    @Benchmark
    public void guavaSplitterIterableForLoop(Blackhole bh) {
        for (String token : SPLITTER.split(string)) {
            bh.consume(token);
        }
    }

    @Benchmark
    public List<String> guavaSplitterStreamToList() {
        return SPLITTER.splitToStream(string)
            .toList();
    }

    @Benchmark
    public void guavaSplitterStreamConsume(Blackhole bh) {
    	SPLITTER.splitToStream(string)
    		.forEach(bh::consume);
    }

}
