package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

/**
 * Benchmark                                    (size)  Mode  Cnt       Score       Error  Units
 * ArraycopyExistingBenchmark.manual                 1  avgt    5       1.519 ±     0.014  ns/op
 * ArraycopyExistingBenchmark.manual                10  avgt    5       3.335 ±     0.087  ns/op
 * ArraycopyExistingBenchmark.manual               100  avgt    5      11.245 ±     0.133  ns/op
 * ArraycopyExistingBenchmark.manual              1000  avgt    5     102.046 ±     3.899  ns/op
 * ArraycopyExistingBenchmark.manual             10000  avgt    5    1069.355 ±    46.188  ns/op
 * ArraycopyExistingBenchmark.manual            100000  avgt    5   10758.194 ±   834.584  ns/op
 * ArraycopyExistingBenchmark.manual           1000000  avgt    5  130173.059 ± 18361.934  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy        1  avgt    5       1.732 ±     0.022  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy       10  avgt    5       2.163 ±     0.032  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy      100  avgt    5      10.796 ±     0.576  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy     1000  avgt    5     102.600 ±     7.593  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy    10000  avgt    5    1663.950 ±   189.850  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy   100000  avgt    5   12019.007 ±  1526.432  ns/op
 * ArraycopyExistingBenchmark.systemArraycopy  1000000  avgt    5  140179.131 ± 31156.510  ns/op
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class ArraycopyExistingBenchmark {

    @Param({ "1", "10", "100", "1000", "10000", "100000", "1000000" })
    private int size;

    private long[] source;
    private long[] destination;

    @Setup
    public void setup() {
        source = new SplittableRandom(42)
            .longs(size)
            .toArray();
        destination = new long[source.length];
    }

    @Benchmark
    public long[] systemArraycopy() {
        System.arraycopy(source, 0, destination, 0, source.length);
        return destination;
    }

    @Benchmark
    public long[] manual() {
        for (var i = 0; i < source.length; i++) {
            destination[i] = source[i];
        }
        return destination;
    }

}