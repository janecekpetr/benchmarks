package com.gitlab.janecekpetr.benchmark;

import com.github.luben.zstd.RecyclingBufferPool;
import com.github.luben.zstd.Zstd;
import com.github.luben.zstd.ZstdOutputStreamNoFinalizer;
import com.gitlab.janecekpetr.util.io.FixedSizeByteArrayOutputStream;
import com.gitlab.janecekpetr.util.io.IOSafeUnaryOperator;
import com.google.common.io.Resources;
import io.airlift.compress.v3.Compressor;
import io.airlift.compress.v3.lz4.Lz4JavaCompressor;
import io.airlift.compress.v3.lzo.LzoCompressor;
import io.airlift.compress.v3.snappy.SnappyCompressor;
import io.airlift.compress.v3.snappy.SnappyFramedOutputStream;
import io.airlift.compress.v3.snappy.SnappyJavaCompressor;
import io.airlift.compress.v3.zstd.ZstdJavaCompressor;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FrameOutputStream;
import net.jpountz.lz4.LZ4FrameOutputStream.BLOCKSIZE;
import net.jpountz.lz4.LZ4FrameOutputStream.FLG;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorOutputStream;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorOutputStream.BlockSize;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorOutputStream.Parameters;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;

/**
 * plaintext:              6017 (1.0)
 * deflated:               1373 (4.382374362709395)
 * gzipped:                1385 (4.3444043321299635)
 * lz4Aircompressor:       2101 (2.863874345549738)
 * lz4Fast:                2100 (2.8652380952380954)
 * lz4High:                1793 (3.355828220858896)
 * lz4Frame:               2115 (2.8449172576832154)
 * lz4FrameKnownSize:      2123 (2.8341968911917097)
 * lzoAircompressor:       2002 (3.0054945054945055)
 * snappyAircompressor:    1959 (3.071465033180194)
 * zstd:                   1474 (4.082089552238806)
 * zstdAircompressor:      1499 (4.014009339559706)
 */
/**
 * Benchmark                                      (compressionStreamType)  (compressionType)         (lz4Type)  Mode  Cnt    Score    Error  Units
 * CompressionBenchmark.fixedSizeByteArrayStream                      N/A                N/A               N/A  avgt    5    0,565 ?  0,001  us/op
 * CompressionBenchmark.byteArrayStream                               N/A                N/A               N/A  avgt    5    1,090 ?  0,006  us/op
 * CompressionBenchmark.deflater                                      N/A                N/A               N/A  avgt    5   83,035 ?  1,922  us/op
 * CompressionBenchmark.aircompressor                                 N/A                LZO               N/A  avgt    5    7,237 ?  0,016  us/op
 * CompressionBenchmark.aircompressor                                 N/A        SNAPPY_JAVA               N/A  avgt    5    6,427 ?  0,033  us/op
 * CompressionBenchmark.aircompressor                                 N/A           LZ4_JAVA               N/A  avgt    5    7,891 ?  0,242  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A       NATIVE_FAST  avgt    5    5,313 ?  0,020  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A  JAVA_UNSAFE_FAST  avgt    5    9,635 ?  0,015  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A    JAVA_SAFE_FAST  avgt    5   13,518 ?  0,457  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A       NATIVE_HIGH  avgt    5  135,384 ?  2,397  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A  JAVA_UNSAFE_HIGH  avgt    5  235,371 ?  0,459  us/op
 * CompressionBenchmark.lz4                                           N/A                N/A    JAVA_SAFE_HIGH  avgt    5  294,948 ?  0,422  us/op
 * CompressionBenchmark.zstd                                          N/A                N/A               N/A  avgt    5   20,081 ?  0,317  us/op
 * CompressionBenchmark.aircompressor                                 N/A          ZSTD_JAVA               N/A  avgt    5   44,129 ?  0,352  us/op
 * CompressionBenchmark.stream                                   DEFLATER                N/A               N/A  avgt    5   83,669 ?  1,054  us/op
 * CompressionBenchmark.stream                                       GZIP                N/A               N/A  avgt    5   81,712 ?  3,346  us/op
 * CompressionBenchmark.stream                                        LZ4                N/A               N/A  avgt    5   17,405 ?  0,129  us/op
 * CompressionBenchmark.stream                             LZ4_KNOWN_SIZE                N/A               N/A  avgt    5   17,375 ?  0,032  us/op
 * CompressionBenchmark.stream                    LZ4_COMMONS_COMPRESSION                N/A               N/A  avgt    5  780,784 ?  2,272  us/op
 * CompressionBenchmark.stream                       SNAPPY_AIRCOMPRESSOR                N/A               N/A  avgt    5   21.448 ±  0.204  us/op
 * CompressionBenchmark.stream                                       ZSTD                N/A               N/A  avgt    5  264,380 ? 12,168  us/op
 * CompressionBenchmark.stream                               ZSTD_POOLING                N/A               N/A  avgt    5  247,574 ? 14,576  us/op
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
public class CompressionBenchmark {

    private byte[] bytes;

    @Setup
    public void prepare() throws IOException {
        URL twitterJson = Resources.getResource("twitter.json");
        bytes = Resources.toByteArray(twitterJson);
    }

    @Benchmark
	public byte[] baselineArrayCopy() {
    	return bytes.clone();
	}

    @Benchmark
	public byte[] baselineFixedSizeByteArrayStream() throws IOException {
	    try (FixedSizeByteArrayOutputStream byteArrayOutputStream = new FixedSizeByteArrayOutputStream(bytes.length)) {
	        byteArrayOutputStream.writeBytes(bytes);
	        return byteArrayOutputStream.buffer();  // Avoids a copy, the underlying buffer was exactly sized.
	    }
	}

	@Benchmark
	public byte[] baselineByteArrayStream() throws IOException {
	    try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytes.length)) {
	        byteArrayOutputStream.writeBytes(bytes);
	        return byteArrayOutputStream.toByteArray();
	    }
	}

	@Benchmark
	public byte[] aircompressor(AircompressorState state) {
	    int maxCompressedLength = state.compressor.maxCompressedLength(bytes.length);
	    byte[] compressed = new byte[maxCompressedLength];
	    int compressedLength = state.compressor.compress(bytes, 0, bytes.length, compressed, 0, maxCompressedLength);
	    return Arrays.copyOf(compressed, compressedLength);
	}

	@Benchmark
	public byte[] lz4(Lz4State state) {
	    return state.lz4Compressor.compress(bytes);
	}

	@Benchmark
	public byte[] zstd() {
	    return Zstd.compress(bytes);
	}

	@Benchmark
    public byte[] deflater() {
        Deflater deflater = new Deflater();
        try {
            deflater.setInput(bytes);
            deflater.finish();

            byte[] output = new byte[bytes.length];
            int compressedLength = deflater.deflate(output);
            return Arrays.copyOf(output, compressedLength);
        } finally {
            deflater.end();
        }
    }

    @Benchmark
    public byte[] stream(StreamingCompressionState state) {
        return compressUsingStream(state.outputStreamSupplier, bytes);
    }

    static byte[] compressUsingStream(IOSafeUnaryOperator<OutputStream> outputStreamSupplier, byte[] bytes) throws AssertionError {
        FixedSizeByteArrayOutputStream byteArrayOutputStream = new FixedSizeByteArrayOutputStream(bytes.length);
        try (OutputStream compressingOutputStream = outputStreamSupplier.apply(byteArrayOutputStream)) {
            compressingOutputStream.write(bytes);
        } catch (IOException e) {
            throw new AssertionError("wut", e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    @State(Scope.Benchmark)
    public static class AircompressorState {

        @Param
        private CompressionType compressionType;
        private Compressor compressor;

        @Setup
        public void setup() {
            compressor = switch (compressionType) {
                case LZ4_JAVA -> new Lz4JavaCompressor();
                case LZO -> new LzoCompressor();
                case SNAPPY_JAVA -> new SnappyJavaCompressor();
                case ZSTD_JAVA -> new ZstdJavaCompressor();
                // not available on Windows
//                case LZ4_NATIVE -> new Lz4NativeCompressor();
//                case SNAPPY_NATIVE -> new SnappyNativeCompressor();
//                case ZSTD_NATIVE -> new ZstdNativeCompressor();
            };
        }

        public enum CompressionType {
        	LZ4_JAVA, /*LZ4_NATIVE,*/ SNAPPY_JAVA, /*SNAPPY_NATIVE,*/ ZSTD_JAVA, /*ZSTD_NATIVE*/ LZO;
        }
    }

    @State(Scope.Benchmark)
    public static class Lz4State {

        private static final FLG.Bits[] LZ4_STREAM_FEATURES = { FLG.Bits.BLOCK_INDEPENDENCE, FLG.Bits.CONTENT_SIZE };

        @Param
        private Lz4Type lz4Type;
        private LZ4Compressor lz4Compressor;

        @Setup
        public void setup() {
            lz4Compressor = switch (lz4Type) {
                case NATIVE_FAST -> LZ4Factory.nativeInstance().fastCompressor();
                case JAVA_UNSAFE_FAST -> LZ4Factory.unsafeInstance().fastCompressor();
                case JAVA_SAFE_FAST -> LZ4Factory.safeInstance().fastCompressor();

                case NATIVE_HIGH -> LZ4Factory.nativeInstance().highCompressor();
                case JAVA_UNSAFE_HIGH -> LZ4Factory.unsafeInstance().highCompressor();
                case JAVA_SAFE_HIGH -> LZ4Factory.safeInstance().highCompressor();
            };
        }

        public enum Lz4Type {
            NATIVE_FAST,
            JAVA_UNSAFE_FAST,
            JAVA_SAFE_FAST,

            NATIVE_HIGH,
            JAVA_UNSAFE_HIGH,
            JAVA_SAFE_HIGH;
        }
    }

    @State(Scope.Benchmark)
    public static class StreamingCompressionState {

        @Param
        private CompressionStreamType compressionStreamType;
        private IOSafeUnaryOperator<OutputStream> outputStreamSupplier;

        @Setup
        public void setup() {
            outputStreamSupplier = switch (compressionStreamType) {
                case DEFLATER -> DeflaterOutputStream::new;
                case GZIP -> GZIPOutputStream::new;
                case LZ4 -> (out -> new LZ4FrameOutputStream(out, BLOCKSIZE.SIZE_64KB));
                case LZ4_KNOWN_SIZE -> this::lz4FramedOutputStreamWithKnownSize;
                case LZ4_COMMONS_COMPRESSION -> (out -> new FramedLZ4CompressorOutputStream(out, new Parameters(BlockSize.K64)));
                case SNAPPY_AIRCOMPRESSOR -> (out -> new SnappyFramedOutputStream(SnappyCompressor.create(), out));
                case ZSTD -> ZstdOutputStreamNoFinalizer::new;
                case ZSTD_POOLING -> (out -> new ZstdOutputStreamNoFinalizer(out, RecyclingBufferPool.INSTANCE));
            };
        }

		private OutputStream lz4FramedOutputStreamWithKnownSize(OutputStream out) throws IOException {
			URL twitterJson = Resources.getResource("twitter.json");
			long knownSize = Resources.asByteSource(twitterJson).size();
			return new LZ4FrameOutputStream(out, BLOCKSIZE.SIZE_64KB, knownSize, Lz4State.LZ4_STREAM_FEATURES);
		}

        public enum CompressionStreamType {
            DEFLATER, GZIP, LZ4, LZ4_KNOWN_SIZE, LZ4_COMMONS_COMPRESSION, SNAPPY_AIRCOMPRESSOR, ZSTD, ZSTD_POOLING;
        }
    }

}