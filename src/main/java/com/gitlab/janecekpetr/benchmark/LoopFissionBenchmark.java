package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;
import java.util.random.RandomGenerator;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class LoopFissionBenchmark {

	@Param({"128", "512", "2048", "4096", "8192"})
	private int size;

	private long[] left;
	private long[] right;

	@Setup
	public void setup() {
		RandomGenerator random = new SplittableRandom(42);
		left = random.longs(size).toArray();
		right = random.longs(size).toArray();
	}

	@Benchmark
	public int fused() {
		int count = 0;
		for (int i = 0; (i < left.length) && (i < right.length); i++) {
			left[i] ^= right[i];
			count += Long.bitCount(left[i]);
		}
		return count;
	}

	@Benchmark
	public int fissured() {
		for (int i = 0; (i < left.length) && (i < right.length); i++) {
			left[i] ^= right[i];
		}

		int count = 0;
		for (long val : left) {
			count += Long.bitCount(val);
		}
		return count;
	}

}
