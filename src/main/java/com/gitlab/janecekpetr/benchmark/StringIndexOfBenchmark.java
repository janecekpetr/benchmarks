package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.concurrent.TimeUnit;

/**
 * Benchmark                              Mode  Cnt   Score   Error  Units
 * indexOfString                          avgt    5   6.736 ± 0.500  ns/op
 * indexOfChar                            avgt    5  11.905 ± 0.457  ns/op
 * indexOfManual                          avgt    5  14.668 ± 0.304  ns/op
 * indexOfManualFixedLength               avgt    5  14.929 ± 0.409  ns/op
 * indexOfManualWithCharArray             avgt    5  16.281 ± 0.446  ns/op
 * indexOfManualWithCharArrayFixedLength  avgt    5  17.673 ± 0.374  ns/op
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class StringIndexOfBenchmark {

	private String haystack;
	private String needleString;
    private char needleChar1;
	private char needleChar2;

	@Setup
	public void prepare() {
		haystack = "The quick brown fox jumps over the lazy dog.";
		needleString = "zy";
        needleChar1 = 'z';
		needleChar2 = 'y';
	}

	@Benchmark
	public int indexOfString() {
		return haystack.indexOf(needleString);
	}

	/** Slow until Java 16. */
	@Benchmark
	public int indexOfChar() {
		int indexOf = haystack.indexOf(needleChar1);
		while ((indexOf != -1) && ((indexOf + 1) < haystack.length()) && ((haystack.charAt(indexOf) + 1) != needleChar2)) {
            indexOf = haystack.indexOf(needleChar1, indexOf + 1);
        }
        return indexOf;
	}

	@Benchmark
	public int indexOfManual() {
		for (int i = 0; i < (haystack.length() - 1); i++) {
			if ((haystack.charAt(i) == needleChar1) && (haystack.charAt(i+1) == needleChar2)) {
				return i;
			}
		}
		return -1;
	}

	@Benchmark
	public int indexOfManualFixedLength() {
		int length = haystack.length() - 1;
		for (int i = 0; i < length; i++) {
			if ((haystack.charAt(i) == needleChar1) && (haystack.charAt(i+1) == needleChar2)) {
				return i;
			}
		}
		return -1;
	}

	@Benchmark
	public int indexOfManualWithCharArray() {
		char[] chars = haystack.toCharArray();
		for (int i = 0; i < (chars.length - 1); i++) {
			if ((chars[i] == needleChar1) && (chars[i+1] == needleChar2)) {
				return i;
			}
		}
		return -1;
	}

	@Benchmark
	public int indexOfManualWithCharArrayFixedLength() {
		char[] chars = haystack.toCharArray();
		int length = chars.length - 1;
		for (int i = 0; i < length; i++) {
            if ((chars[i] == needleChar1) && (chars[i+1] == needleChar2)) {
				return i;
			}
		}
		return -1;
	}

}
