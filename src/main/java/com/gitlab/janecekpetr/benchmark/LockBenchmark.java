package com.gitlab.janecekpetr.benchmark;

import com.gitlab.janecekpetr.util.lock.SpinLock;
import com.gitlab.janecekpetr.util.lock.SpinLockWithPause;
import com.gitlab.janecekpetr.util.lock.SpinLockWithYield;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.CompilerControl;
import org.openjdk.jmh.annotations.CompilerControl.Mode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.StampedLock;

// The results from a write-only workload with no fake work follow:
/*
 * 1 THREAD
 * Benchmark                   Score      Error  Units
 * baselineNoLocking       665284236 ?   263456  ops/s
 * atomicInteger           209672408 ?   999603  ops/s
 *
 * synchronizedLockObject   59599903 ?    22595  ops/s
 * reentrantLock            63392399 ?    21763  ops/s
 * reentrantRWLock          63494492 ?  1010856  ops/s
 * semaphore                60504013 ?    14868  ops/s
 * stampedLock              67697148 ?    14202  ops/s
 * stampedLockAsRWLock      63621800 ?    12545  ops/s
 * stampedLockAsWLock       64509372 ?   239454  ops/s
 *
 * spinlock                 83172689 ?    38813  ops/s
 * spinlockWithPause        83182836 ?    24298  ops/s
 * spinlockWithYield        83186316 ?    29721  ops/s
 *
 * fairReentrantLock        65465389 ?    19009  ops/s
 * fairReentrantRWLock      63394149 ?    18038  ops/s
 * fairSemaphore            60501028 ?    28106  ops/s
 */
/*
 * 2 THREADS
 * Benchmark                   Score     Error  Units
 * baselineNoLocking      1295348424 ?  560201  ops/s
 * atomicInteger            52398520 ?  763290  ops/s
 *
 * synchronizedLockObject   49061279 ?  492625  ops/s
 * reentrantLock            28604240 ?  196237  ops/s
 * reentrantRWLock          24849346 ?  235837  ops/s
 * semaphore                27093685 ?  220150  ops/s
 * stampedLock              30810576 ?  218123  ops/s
 * stampedLockAsRWLock      28029269 ?  189191  ops/s
 * stampedLockAsWLock       29300049 ?   78125  ops/s
 *
 * spinlock                 10526581 ? 1639702  ops/s
 * spinlockWithPause         9397829 ?  436982  ops/s
 * spinlockWithYield        66007538 ?  505226  ops/s
 *
 * fairReentrantLock          198045 ?    6446  ops/s
 * fairReentrantRWLock        195800 ?    7648  ops/s
 * fairSemaphore              179571 ?    7347  ops/s
 */
/*
 * 4 THREADS
 * Benchmark                   Score      Error  Units
 * baselineNoLocking      2443450537 ? 16806959  ops/s
 * atomicInteger            52519834 ?   219832  ops/s
 *
 * synchronizedLockObject   39144651 ?   124144  ops/s
 * reentrantLock            45144638 ?   120985  ops/s
 * reentrantRWLock          41203644 ?   210337  ops/s
 * semaphore                35141403 ?   168595  ops/s
 * stampedLock              48862162 ?   286722  ops/s
 * stampedLockAsRWLock      40175001 ?   321727  ops/s
 * stampedLockAsWLock       43904605 ?   513164  ops/s
 *
 * spinlock                  6496875 ?    76676  ops/s
 * spinlockWithPause         6170789 ?    52938  ops/s
 * spinlockWithYield        36012604 ?   510465  ops/s
 *
 * fairReentrantLock          178092 ?     5126  ops/s
 * fairReentrantRWLock        170072 ?     5608  ops/s
 * fairSemaphore              168729 ?     2530  ops/s
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@Threads(Threads.MAX)
@State(Scope.Benchmark)
public class LockBenchmark {

	private static final int WRITE_THREADS = 1;
    private static final int READ_THREADS = 3;

	private static final int WRITE_FAKE_WORK_AMOUNT = 100;
    private static final int READ_FAKE_WORK_AMOUNT = 10;

    private int count;
    private AtomicInteger atomicCount;

    @Setup
    public void setup() {
        count = 0;
        atomicCount = new AtomicInteger(0);
    }

    @State(Scope.Benchmark)
	public static class FairLockState {

	    ReentrantLock reentrantLock;
	    ReentrantReadWriteLock reentrantRWLock;
	    Semaphore semaphore;

	    @Param({ "false", "true" })
	    private boolean fair;

	    @Setup
	    public void setup() {
	        reentrantLock = new ReentrantLock(fair);
	        reentrantRWLock = new ReentrantReadWriteLock(fair);
	        semaphore = new Semaphore(1, fair);
	    }
	}

	@State(Scope.Benchmark)
    public static class UnfairLockState {

        Object lockObject;
        StampedLock stampedLock;
        Lock stampedLockAsWriteLock;
        Lock stampedLockAsReadLock;
		ReadWriteLock stampedLockAsRWLock;


        @Setup
        public void setup() {
            lockObject = new Object();
            stampedLock = new StampedLock();
            stampedLockAsWriteLock = stampedLock.asWriteLock();
            stampedLockAsReadLock = stampedLock.asReadLock();
            stampedLockAsRWLock = stampedLock.asReadWriteLock();
        }
    }

    @State(Scope.Benchmark)
    public static class SpinLockState {

    	Lock spinLock;

    	@Param
    	private SpinLockType spinLockType;

        @Setup
        public void setup() {
        	spinLock = switch (spinLockType) {
				case NO_OP -> new SpinLock();
				case PAUSE -> new SpinLockWithPause();
				case YIELD -> new SpinLockWithYield();
			};
        }

        public enum SpinLockType {
        	NO_OP, PAUSE, YIELD;
        }
    }

    @Benchmark
    @Group("baseline")
    @GroupThreads(WRITE_THREADS)
    public int baselineNoLockingWrite() {
    	int valueToWrite = computeValueToWrite();
        return performWrite(valueToWrite);
    }

    @Benchmark
    @Group("baseline")
    @GroupThreads(READ_THREADS)
    public int baselineNoLockingRead() {
        int value = performRead();
        useReadValue(value);
		return value;
    }

    @Benchmark
    @Group("atomic")
    @GroupThreads(WRITE_THREADS)
    public int atomicIntegerWrite() {
    	int valueToWrite = computeValueToWrite();
        return performAtomicWrite(valueToWrite);
    }

    @Benchmark
    @Group("atomic")
    @GroupThreads(READ_THREADS)
    public int atomicIntegerRead() {
        int value = performAtomicRead();
        useReadValue(value);
		return value;
    }

    @Benchmark
    @Group("synchronized")
    @GroupThreads(WRITE_THREADS)
    public int synchronizedLockObjectWrite(UnfairLockState state) {
    	int valueToWrite = computeValueToWrite();
        synchronized (state.lockObject) {
            return performWrite(valueToWrite);
        }
    }

    @Benchmark
    @Group("synchronized")
    @GroupThreads(READ_THREADS)
    public int synchronizedLockObjectRead(UnfairLockState state) {
    	int value;
        synchronized (state.lockObject) {
            value = performRead();
        }
        useReadValue(value);
        return value;
    }

    @Benchmark
    @Group("reentrantLock")
    @GroupThreads(WRITE_THREADS)
	public int reentrantLockWrite(FairLockState state) {
		return writeWithLock(state.reentrantLock);
	}

    @Benchmark
    @Group("reentrantLock")
    @GroupThreads(READ_THREADS)
	public int reentrantLockRead(FairLockState state) {
		return readWithLock(state.reentrantLock);
	}

	@Benchmark
    @Group("reentrantRWLock")
    @GroupThreads(WRITE_THREADS)
	public int reentrantRWLockWrite(FairLockState state) {
		return writeWithLock(state.reentrantRWLock.writeLock());
	}

	@Benchmark
    @Group("reentrantRWLock")
    @GroupThreads(READ_THREADS)
	public int reentrantRWLockRead(FairLockState state) {
		return readWithLock(state.reentrantRWLock.readLock());
	}

	@Benchmark
    @Group("semaphore")
    @GroupThreads(WRITE_THREADS)
	public int semaphoreWrite(FairLockState state) {
		int valueToWrite = computeValueToWrite();
		state.semaphore.acquireUninterruptibly();
	    try {
	        return performWrite(valueToWrite);
	    } finally {
	    	state.semaphore.release();
	    }
	}

	@Benchmark
    @Group("semaphore")
    @GroupThreads(READ_THREADS)
	public int semaphoreRead(FairLockState state) {
		int value;
		state.semaphore.acquireUninterruptibly();
	    try {
	        value = performRead();
	    } finally {
	    	state.semaphore.release();
	    }
	    useReadValue(value);
	    return value;
	}

	@Benchmark
    @Group("stamped")
    @GroupThreads(WRITE_THREADS)
    public int stampedLockWrite(UnfairLockState state) {
		int valueToWrite = computeValueToWrite();
        long stamp = state.stampedLock.writeLock();
        try {
            return performWrite(valueToWrite);
        } finally {
        	state.stampedLock.unlockWrite(stamp);
        }
    }

	@Benchmark
    @Group("stamped")
    @GroupThreads(READ_THREADS)
    public int stampedLockRead(UnfairLockState state) {
		int value;
        long stamp = state.stampedLock.readLock();
        try {
            value = performRead();
        } finally {
        	state.stampedLock.unlockRead(stamp);
        }
        useReadValue(value);
        return value;
    }

	@Benchmark
    @Group("stampedAsLock")
    @GroupThreads(WRITE_THREADS)
    public int stampedLockAsLockWrite(UnfairLockState state) {
    	return writeWithLock(state.stampedLockAsWriteLock);
    }

	@Benchmark
    @Group("stampedAsLock")
    @GroupThreads(READ_THREADS)
    public int stampedLockAsLockRead(UnfairLockState state) {
    	return readWithLock(state.stampedLockAsReadLock);
    }

	@Benchmark
    @Group("stampedAsRWLock")
    @GroupThreads(WRITE_THREADS)
    public int stampedLockAsRWLockWrite(UnfairLockState state) {
    	return writeWithLock(state.stampedLockAsRWLock.writeLock());
    }

	@Benchmark
    @Group("stampedAsRWLock")
    @GroupThreads(READ_THREADS)
    public int stampedLockAsRWLockRead(UnfairLockState state) {
    	return readWithLock(state.stampedLockAsRWLock.readLock());
    }

	@Benchmark
    @Group("stampedOptimistic")
    @GroupThreads(WRITE_THREADS)
    public int stampedLockOptimisticWrite(UnfairLockState state) {
		int valueToWrite = computeValueToWrite();
        long stamp = state.stampedLock.writeLock();
        try {
            return performWrite(valueToWrite);
        } finally {
        	state.stampedLock.unlockWrite(stamp);
        }
    }

	@Benchmark
    @Group("stampedOptimistic")
    @GroupThreads(READ_THREADS)
    public int stampedLockOptimisticRead(UnfairLockState state) {
		int value;

		long stamp = state.stampedLock.tryOptimisticRead();
		try {
			for (;; stamp = state.stampedLock.readLock()) {
				if (stamp == 0) {
					// Optimistic read failed, the lock is write-locked.
					continue;
				}
				value = performRead();	// possibly racy read
				if (!state.stampedLock.validate(stamp)) {
					// Optimistic read failed, someone write-locked it in the meantime.
					continue;
				}
				// OK, either the optimistic read succeeded, or we read-locked and read the value, return.
				break;
			}
		} finally {
			if (StampedLock.isReadLockStamp(stamp)) {
				state.stampedLock.unlockRead(stamp);
			}
		}

		useReadValue(value);
		return value;
    }

    @Benchmark
    @Group("spinLock")
    @GroupThreads(WRITE_THREADS)
    public int spinlockWrite(SpinLockState state) {
    	return writeWithLock(state.spinLock);
    }

	@Benchmark
    @Group("spinLock")
    @GroupThreads(READ_THREADS)
    public int spinlockRead(SpinLockState state) {
    	return readWithLock(state.spinLock);
    }

	/** Performs a costly operation without a lock, then exclusively locks and writes a value. */
    @CompilerControl(Mode.INLINE)
	private int writeWithLock(Lock lock) {
    	int valueToWrite = computeValueToWrite();
		lock.lock();
	    try {
	        return performWrite(valueToWrite);
	    } finally {
	    	lock.unlock();
	    }
	}

	@SuppressWarnings("static-method")
	private int computeValueToWrite() {
    	Blackhole.consumeCPU(WRITE_FAKE_WORK_AMOUNT);
    	return WRITE_FAKE_WORK_AMOUNT;	// TODO Does this ever matter, should we actually read something from state?
	}

    private int performWrite(@SuppressWarnings("unused") int value) {
        return ++count;
    }

	private int performAtomicWrite(@SuppressWarnings("unused") int value) {
		return atomicCount.incrementAndGet();
	}

	/** Reads a value under a read-lock, then uses the value for a little while. */
    @CompilerControl(Mode.INLINE)
	private int readWithLock(Lock lock) {
    	int value;
		lock.lock();
	    try {
	        value = performRead();
	    } finally {
	    	lock.unlock();
	    }
	    useReadValue(value);
    	return value;
	}

	private int performRead() {
		return count;
	}

	private int performAtomicRead() {
		return atomicCount.get();
	}

	@SuppressWarnings({ "unused", "static-method" })
	private void useReadValue(int value) {
    	Blackhole.consumeCPU(READ_FAKE_WORK_AMOUNT);
	}
}