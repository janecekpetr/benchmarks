package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.Arrays;
import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

/**
 * Benchmark                            (size)  Mode  Cnt        Score        Error  Units
 * ArraycopyBenchmark.arrayClone             1  avgt    5        2.670 ±      0.029  ns/op
 * ArraycopyBenchmark.arrayClone            10  avgt    5        3.851 ±      0.016  ns/op
 * ArraycopyBenchmark.arrayClone           100  avgt    5       22.167 ±      0.172  ns/op
 * ArraycopyBenchmark.arrayClone          1000  avgt    5     1227.801 ±    123.138  ns/op
 * ArraycopyBenchmark.arrayClone         10000  avgt    5     2101.847 ±     85.790  ns/op
 * ArraycopyBenchmark.arrayClone        100000  avgt    5    18478.744 ±    134.565  ns/op
 * ArraycopyBenchmark.arrayClone       1000000  avgt    5   371126.219 ± 611511.607  ns/op
 * ArraycopyBenchmark.arraysCopyOf           1  avgt    5        2.699 ±      0.017  ns/op
 * ArraycopyBenchmark.arraysCopyOf          10  avgt    5        3.884 ±      0.070  ns/op
 * ArraycopyBenchmark.arraysCopyOf         100  avgt    5       20.041 ±      0.297  ns/op
 * ArraycopyBenchmark.arraysCopyOf        1000  avgt    5     1523.268 ±    147.248  ns/op
 * ArraycopyBenchmark.arraysCopyOf       10000  avgt    5     2098.409 ±     57.317  ns/op
 * ArraycopyBenchmark.arraysCopyOf      100000  avgt    5    17725.527 ±   1229.432  ns/op
 * ArraycopyBenchmark.arraysCopyOf     1000000  avgt    5   146906.811 ±   7618.949  ns/op
 * ArraycopyBenchmark.arraysStream           1  avgt    5       17.396 ±      0.185  ns/op
 * ArraycopyBenchmark.arraysStream          10  avgt    5       22.385 ±      0.157  ns/op
 * ArraycopyBenchmark.arraysStream         100  avgt    5       69.521 ±      0.263  ns/op
 * ArraycopyBenchmark.arraysStream        1000  avgt    5     1194.769 ±     98.403  ns/op
 * ArraycopyBenchmark.arraysStream       10000  avgt    5    12907.298 ±    187.148  ns/op
 * ArraycopyBenchmark.arraysStream      100000  avgt    5   162323.374 ±   7369.709  ns/op
 * ArraycopyBenchmark.arraysStream     1000000  avgt    5  1625632.884 ±  99400.697  ns/op
 * ArraycopyBenchmark.manual                 1  avgt    5        2.834 ±      0.089  ns/op
 * ArraycopyBenchmark.manual                10  avgt    5        5.932 ±      0.036  ns/op
 * ArraycopyBenchmark.manual               100  avgt    5       28.270 ±      0.151  ns/op
 * ArraycopyBenchmark.manual              1000  avgt    5     1006.290 ±    899.133  ns/op
 * ArraycopyBenchmark.manual             10000  avgt    5     2749.426 ±     30.732  ns/op
 * ArraycopyBenchmark.manual            100000  avgt    5    24814.212 ±   8115.661  ns/op
 * ArraycopyBenchmark.manual           1000000  avgt    5   222021.062 ±  21481.804  ns/op
 * ArraycopyBenchmark.systemArraycopy        1  avgt    5        2.697 ±      0.084  ns/op
 * ArraycopyBenchmark.systemArraycopy       10  avgt    5        3.856 ±      0.010  ns/op
 * ArraycopyBenchmark.systemArraycopy      100  avgt    5       19.920 ±      0.234  ns/op
 * ArraycopyBenchmark.systemArraycopy     1000  avgt    5     1191.605 ±    154.753  ns/op
 * ArraycopyBenchmark.systemArraycopy    10000  avgt    5     2089.584 ±    284.407  ns/op
 * ArraycopyBenchmark.systemArraycopy   100000  avgt    5    19149.650 ±    152.284  ns/op
 * ArraycopyBenchmark.systemArraycopy  1000000  avgt    5   171919.806 ± 121609.141  ns/op
 */
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class ArraycopyBenchmark {

    @Param({ "1", "10", "100", "1000", "10000", "100000", "1000000" })
    private int size;

    private long[] array;

    @Setup
    public void setup() {
        array = new SplittableRandom(42)
            .longs(size)
            .toArray();
    }

    @Benchmark
    public long[] arrayClone() {
        return array.clone();
    }

    @Benchmark
    public long[] arraysCopyOf() {
        return Arrays.copyOf(array, array.length);
    }

    @Benchmark
    public long[] arraysStream() {
        return Arrays.stream(array)
            .toArray();
    }

    @Benchmark
    public long[] systemArraycopy() {
        var copy = new long[array.length];
        System.arraycopy(array, 0, copy, 0, array.length);
        return copy;
    }

    @Benchmark
    public long[] manual() {
        var copy = new long[array.length];
        for (var i = 0; i < array.length; i++) {
             copy[i] = array[i];
        }
        return copy;
    }

}