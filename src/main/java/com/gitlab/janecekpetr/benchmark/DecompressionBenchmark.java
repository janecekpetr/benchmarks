package com.gitlab.janecekpetr.benchmark;

import static com.gitlab.janecekpetr.benchmark.CompressionBenchmark.compressUsingStream;

import com.github.luben.zstd.Zstd;
import com.github.luben.zstd.ZstdInputStreamNoFinalizer;
import com.gitlab.janecekpetr.util.io.IOSafeUnaryOperator;
import com.google.common.io.Resources;
import io.airlift.compress.v3.Compressor;
import io.airlift.compress.v3.Decompressor;
import io.airlift.compress.v3.lz4.Lz4Compressor;
import io.airlift.compress.v3.lz4.Lz4JavaDecompressor;
import io.airlift.compress.v3.lz4.Lz4NativeDecompressor;
import io.airlift.compress.v3.lzo.LzoCompressor;
import io.airlift.compress.v3.lzo.LzoDecompressor;
import io.airlift.compress.v3.snappy.SnappyCompressor;
import io.airlift.compress.v3.snappy.SnappyJavaDecompressor;
import io.airlift.compress.v3.snappy.SnappyNativeDecompressor;
import io.airlift.compress.v3.zstd.ZstdCompressor;
import io.airlift.compress.v3.zstd.ZstdJavaDecompressor;
import io.airlift.compress.v3.zstd.ZstdNativeDecompressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import net.jpountz.lz4.LZ4FrameInputStream;
import net.jpountz.lz4.LZ4FrameOutputStream;
import net.jpountz.lz4.LZ4FrameOutputStream.BLOCKSIZE;
import net.jpountz.lz4.LZ4FrameOutputStream.FLG;
import net.jpountz.lz4.LZ4SafeDecompressor;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorInputStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.zip.DataFormatException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Benchmark                                                    Mode  Cnt    Score    Error  Units
 * DecompressionBenchmark.byteArrayStream                       avgt    5    0.425 ±  0.003  us/op
 * DecompressionBenchmark.gzipStream                            avgt    5   11.802 ±  2.504  us/op
 * DecompressionBenchmark.inflaterManual                        avgt    5    9.446 ±  2.143  us/op
 * DecompressionBenchmark.inflaterStream                        avgt    5   10.845 ±  0.127  us/op
 * DecompressionBenchmark.lz4Aircompressor                      avgt    5    2.223 ±  0.038  us/op
 * DecompressionBenchmark.lz4FastFastManual                     avgt    5    1.277 ±  0.007  us/op
 * DecompressionBenchmark.lz4FastSafeManual                     avgt    5    1.348 ±  0.010  us/op
 * DecompressionBenchmark.lz4HighFastManual                     avgt    5    1.098 ±  0.010  us/op
 * DecompressionBenchmark.lz4HighSafeManual                     avgt    5    1.167 ±  0.025  us/op
 * DecompressionBenchmark.lz4Stream                             avgt    5  378.514 ±  9.353  us/op
 * DecompressionBenchmark.lz4StreamCommonsCompression           avgt    5   46.827 ±  3.579  us/op
 * DecompressionBenchmark.lz4StreamKnownSize                    avgt    5   11.263 ±  0.102  us/op
 * DecompressionBenchmark.lz4StreamKnownSizeCommonsCompression  avgt    5   49.540 ± 10.479  us/op
 * DecompressionBenchmark.lzoAircompressor                      avgt    5    3.110 ±  0.020  us/op
 * DecompressionBenchmark.snappyAircompressor                   avgt    5    3.425 ±  0.026  us/op
 * DecompressionBenchmark.zstdAircompressor                     avgt    5   20.858 ±  0.526  us/op
 * DecompressionBenchmark.zstdManual                            avgt    5    6.588 ±  1.209  us/op
 * DecompressionBenchmark.zstdStream                            avgt    5   42.101 ±  5.591  us/op
*/
/**
 * Benchmark                                                    Mode  Cnt    Score   Error  Units
 * DecompressionBenchmark.byteArrayStream                       avgt   15    0,503 ? 0,006  us/op
 * DecompressionBenchmark.gzipStream                            avgt   15   15,134 ? 0,116  us/op
 * DecompressionBenchmark.inflaterManual                        avgt   15   12,790 ? 0,828  us/op
 * DecompressionBenchmark.inflaterStream                        avgt   15   14,117 ? 0,062  us/op
 * DecompressionBenchmark.lz4AircompressorJava                  avgt   15    2,505 ? 0,062  us/op
 * DecompressionBenchmark.lz4ManualFastFast                     avgt   15    1,824 ? 0,009  us/op
 * DecompressionBenchmark.lz4ManualFastSafe                     avgt   15    1,837 ? 0,028  us/op
 * DecompressionBenchmark.lz4ManualHighFast                     avgt   15    1,331 ? 0,013  us/op
 * DecompressionBenchmark.lz4ManualHighSafe                     avgt   15    1,338 ? 0,015  us/op
 * DecompressionBenchmark.lz4Stream                             avgt   15  338,099 ? 1,900  us/op
 * DecompressionBenchmark.lz4StreamCommonsCompression           avgt   15  127,773 ? 0,123  us/op
 * DecompressionBenchmark.lz4StreamKnownSize                    avgt   15   13,260 ? 0,013  us/op
 * DecompressionBenchmark.lz4StreamKnownSizeCommonsCompression  avgt   15  128,095 ? 0,317  us/op
 * DecompressionBenchmark.lzoAircompressor                      avgt   15    3,161 ? 0,013  us/op
 * DecompressionBenchmark.snappyAircompressorJava               avgt   15    3,857 ? 0,019  us/op
 * DecompressionBenchmark.zstdAircompressorJava                 avgt   15   19,747 ? 0,122  us/op
 * DecompressionBenchmark.zstdManual                            avgt   15    6,473 ? 0,026  us/op
 * DecompressionBenchmark.zstdStream                            avgt   15   16,417 ? 0,045  us/op
*/
@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
public class DecompressionBenchmark {

    private byte[] rawBytes;
    private byte[] deflatedBytes;
    private byte[] gzippedBytes;
    private byte[] lz4FastBytes;
    private byte[] lz4HighBytes;
    private byte[] lz4FrameBytes;
    private byte[] lz4FrameKnownSizeBytes;
    private byte[] lz4AircompressedBytes;
    private byte[] lzoAircompressedBytes;
    private byte[] snappyAircompressedBytes;
    private byte[] zstdBytes;
    private byte[] zstdAircompressedBytes;

    @Setup
    public void prepare() throws IOException {
        URL twitterJson = Resources.getResource("twitter.json");
        rawBytes = Resources.toByteArray(twitterJson);

        deflatedBytes = compressUsingStream(DeflaterOutputStream::new, rawBytes);
        gzippedBytes = compressUsingStream(GZIPOutputStream::new, rawBytes);
        lz4FastBytes = LZ4Factory.fastestInstance().fastCompressor().compress(rawBytes);
        lz4HighBytes = LZ4Factory.fastestInstance().highCompressor().compress(rawBytes);
        lz4FrameBytes = compressUsingStream(LZ4FrameOutputStream::new, rawBytes);
        lz4FrameKnownSizeBytes = compressUsingStream(out -> new LZ4FrameOutputStream(out, BLOCKSIZE.SIZE_64KB, rawBytes.length, FLG.Bits.BLOCK_INDEPENDENCE, FLG.Bits.CONTENT_SIZE), rawBytes);
        lz4AircompressedBytes = aircompress(Lz4Compressor.create(), rawBytes);
        lzoAircompressedBytes = aircompress(new LzoCompressor(), rawBytes);
        snappyAircompressedBytes = aircompress(SnappyCompressor.create(), rawBytes);
        zstdBytes = Zstd.compress(rawBytes);
        zstdAircompressedBytes = aircompress(ZstdCompressor.create(), rawBytes);

        System.out.println("Twitter.json:\t\t" + rawBytes.length);
        System.out.println("deflated:\t\t" + printRatio(rawBytes, deflatedBytes));
        System.out.println("gzipped:\t\t" + printRatio(rawBytes, gzippedBytes));
        System.out.println("lz4Fast:\t\t" + printRatio(rawBytes, lz4FastBytes));
        System.out.println("lz4High:\t\t" + printRatio(rawBytes, lz4HighBytes));
        System.out.println("lz4Frame:\t\t" + printRatio(rawBytes, lz4FrameBytes));
        System.out.println("lz4FrameKnownSize:\t" + printRatio(rawBytes, lz4FrameKnownSizeBytes));
        System.out.println("lz4Aircompressor:\t" + printRatio(rawBytes, lz4AircompressedBytes));
        System.out.println("lzoAircompressor:\t" + printRatio(rawBytes, lzoAircompressedBytes));
        System.out.println("snappyAircompressor:\t" + printRatio(rawBytes, snappyAircompressedBytes));
        System.out.println("zstd:\t\t\t" + printRatio(rawBytes, zstdBytes));
        System.out.println("zstdAircompressor:\t" + printRatio(rawBytes, zstdAircompressedBytes));
    }

    private static String printRatio(byte[] raw, byte[] compressed) {
    	return compressed.length + " (" + (1.0 * raw.length / compressed.length) + ")";
    }

    private static byte[] aircompress(Compressor compressor, byte[] bytes) {
        int maxCompressedLength = compressor.maxCompressedLength(bytes.length);
        byte[] compressed = new byte[maxCompressedLength];
        int compressedLength = compressor.compress(bytes, 0, bytes.length, compressed, 0, maxCompressedLength);
        return Arrays.copyOf(compressed, compressedLength);
    }

    private byte[] airdecompress(Decompressor decompressor, byte[] compressed) {
        byte[] decompressed = new byte[rawBytes.length];
        decompressor.decompress(compressed, 0, compressed.length, decompressed, 0, rawBytes.length);
        return decompressed;
    }

    @Benchmark
    public byte[] byteArrayStream() throws IOException {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(rawBytes)) {
            return byteArrayInputStream.readAllBytes();
        }
    }

    @Benchmark
    public byte[] inflaterManual() throws DataFormatException {
        Inflater inflater = new Inflater();
        try {
            inflater.setInput(deflatedBytes);

            byte[] output = new byte[rawBytes.length];
            int decompressedLength = inflater.inflate(output);
            return Arrays.copyOf(output, decompressedLength);
        } finally {
            inflater.end();
        }
    }

    @Benchmark
    public byte[] inflaterStream() {
        return decompressUsingStream(InflaterInputStream::new, deflatedBytes);
    }

    @Benchmark
    public byte[] gzipStream() {
        return decompressUsingStream(GZIPInputStream::new, gzippedBytes);
    }

    @Benchmark
    public byte[] lz4AircompressorJava() {
        return airdecompress(new Lz4JavaDecompressor(), lz4AircompressedBytes);
    }

    @Benchmark
    public byte[] lz4AircompressorNative() {
        return airdecompress(new Lz4NativeDecompressor(), lz4AircompressedBytes);
    }

    @Benchmark
    public byte[] lz4ManualFastSafe() {
        LZ4SafeDecompressor lz4Decompressor = LZ4Factory.fastestInstance().safeDecompressor();
        return lz4Decompressor.decompress(lz4FastBytes, rawBytes.length);
    }

    @Benchmark
    public byte[] lz4ManualHighSafe() {
        LZ4SafeDecompressor lz4Decompressor = LZ4Factory.fastestInstance().safeDecompressor();
        return lz4Decompressor.decompress(lz4HighBytes, rawBytes.length);
    }

    @Benchmark
    public byte[] lz4ManualFastFast() {
        LZ4FastDecompressor lz4Decompressor = LZ4Factory.fastestInstance().fastDecompressor();
        return lz4Decompressor.decompress(lz4FastBytes, rawBytes.length);
    }

    @Benchmark
    public byte[] lz4ManualHighFast() {
        LZ4FastDecompressor lz4Decompressor = LZ4Factory.fastestInstance().fastDecompressor();
        return lz4Decompressor.decompress(lz4HighBytes, rawBytes.length);
    }

    @Benchmark
    public byte[] lz4Stream() {
        return decompressUsingStream(LZ4FrameInputStream::new, lz4FrameBytes);
    }

    @Benchmark
    public byte[] lz4StreamCommonsCompression() {
        return decompressUsingStream(FramedLZ4CompressorInputStream::new, lz4FrameBytes);
    }

    @Benchmark
    public byte[] lz4StreamKnownSize() {
        return decompressUsingStream(LZ4FrameInputStream::new, lz4FrameKnownSizeBytes);
    }

    @Benchmark
    public byte[] lz4StreamKnownSizeCommonsCompression() {
        return decompressUsingStream(FramedLZ4CompressorInputStream::new, lz4FrameKnownSizeBytes);
    }

    @Benchmark
    public byte[] lzoAircompressor() {
        return airdecompress(new LzoDecompressor(), lzoAircompressedBytes);
    }

    @Benchmark
    public byte[] snappyAircompressorJava() {
        return airdecompress(new SnappyJavaDecompressor(), snappyAircompressedBytes);
    }

    @Benchmark
    public byte[] snappyAircompressorNative() {
        return airdecompress(new SnappyNativeDecompressor(), snappyAircompressedBytes);
    }

    @Benchmark
    public byte[] zstdManual() {
        return Zstd.decompress(zstdBytes, rawBytes.length);
    }

    @Benchmark
    public byte[] zstdStream() {
        return decompressUsingStream(ZstdInputStreamNoFinalizer::new, zstdBytes);
    }

    @Benchmark
    public byte[] zstdAircompressorJava() {
        return airdecompress(new ZstdJavaDecompressor(), zstdAircompressedBytes);
    }

    @Benchmark
    public byte[] zstdAircompressorNative() {
        return airdecompress(new ZstdNativeDecompressor(), zstdAircompressedBytes);
    }

    private static byte[] decompressUsingStream(IOSafeUnaryOperator<InputStream> inputStreamSupplier, byte[] bytes) throws AssertionError {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        try (InputStream decompressingInputStream = inputStreamSupplier.apply(byteArrayInputStream)) {
            return decompressingInputStream.readAllBytes();
        } catch (IOException e) {
            throw new AssertionError("wut", e);
        }
    }

}