package com.gitlab.janecekpetr.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

@Fork(1)
@Warmup(iterations = 2, time = 3)
@Measurement(iterations = 5, time = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class DurationFormatBenchmark {

	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("######00");
	private static final DecimalFormat DECIMAL_FORMAT2 = new DecimalFormat("00");
	private static final DecimalFormat DECIMAL_FORMAT3 = new DecimalFormat("00.0##");

	private long durationInMillis;

    @Setup
    public void prepare() {
        durationInMillis = new SplittableRandom(42).nextLong(100_000_000);
    }

	@Benchmark
	public String original() {
        // Calculate how many seconds, and don't lose any information ...
        BigDecimal bigSeconds = BigDecimal.valueOf(Math.abs(durationInMillis)).divide(new BigDecimal(1000));
        // Calculate the minutes, and round to lose the seconds
        int minutes = bigSeconds.intValue() / 60;
        // Remove the minutes from the seconds, to just have the remainder of seconds
        double dMinutes = minutes;
        double seconds = bigSeconds.doubleValue() - dMinutes * 60;
        // Now compute the number of full hours, and change 'minutes' to hold the remaining minutes
        int hours = minutes / 60;
        minutes = minutes - (hours * 60);

        // Format the string, and have at least 2 digits for the hours, minutes and whole seconds,
        // and between 3 and 6 digits for the fractional part of the seconds...
        return new DecimalFormat("######00").format(hours) + ':' + new DecimalFormat("00").format(minutes) + ':' + new DecimalFormat("00.0##").format(seconds);
    }

	@Benchmark
	public String originalWithStaticFormats() {
        // Calculate how many seconds, and don't lose any information ...
        BigDecimal bigSeconds = BigDecimal.valueOf(Math.abs(durationInMillis)).divide(new BigDecimal(1000));
        // Calculate the minutes, and round to lose the seconds
        int minutes = bigSeconds.intValue() / 60;
        // Remove the minutes from the seconds, to just have the remainder of seconds
        double dMinutes = minutes;
        double seconds = bigSeconds.doubleValue() - dMinutes * 60;
        // Now compute the number of full hours, and change 'minutes' to hold the remaining minutes
        int hours = minutes / 60;
        minutes = minutes - (hours * 60);

        // Format the string, and have at least 2 digits for the hours, minutes and whole seconds,
        // and between 3 and 6 digits for the fractional part of the seconds...
        return DECIMAL_FORMAT.format(hours) + ':' + DECIMAL_FORMAT2.format(minutes) + ':' + DECIMAL_FORMAT3.format(seconds);
    }

	@Benchmark
	public String originalSimplified() {
        long seconds = durationInMillis / 1000;
        long millis = durationInMillis - (seconds * 1000);
        long minutes = seconds / 60;
        seconds = seconds - (minutes * 60);
        long hours = minutes / 60;
        minutes = minutes - (hours * 60);

        return DECIMAL_FORMAT.format(hours) + ':' + DECIMAL_FORMAT2.format(minutes) + ':' + DECIMAL_FORMAT3.format(seconds + millis/1000d);
    }

	@Benchmark
	public String stringFormat() {
        long seconds = durationInMillis / 1000;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60));
        long q = durationInMillis % 1000;
        return String.format("%02d:%02d:%02d.%03d", h, m, s, q);
    }

	@Benchmark
	public String stringBuilder() {
        long seconds = durationInMillis / 1000;
        long millisOnly = durationInMillis - (seconds * 1000);
        long minutes = seconds / 60;
        long secondsOnly = seconds - (minutes * 60);
        long hours = minutes / 60;
        long minutesOnly = minutes - (hours * 60);

        StringBuilder sb = new StringBuilder();
        if (hours < 10) {
        	sb.append('0');
        }
        sb.append(hours)
        	.append(':');
        if (minutesOnly < 10) {
        	sb.append('0');
        }
        sb.append(minutesOnly)
    		.append(':');
        if (secondsOnly < 10) {
        	sb.append('0');
        }
        sb.append(secondsOnly)
    		.append('.');
        if (millisOnly < 100) {
        	sb.append('0');
        	if (millisOnly < 10) {
        		sb.append('0');
        	}
        }
    	sb.append(millisOnly);
        return sb.toString();
    }

}
