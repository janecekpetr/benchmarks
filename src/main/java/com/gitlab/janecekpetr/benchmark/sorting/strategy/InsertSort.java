package com.gitlab.janecekpetr.benchmark.sorting.strategy;

import java.util.Arrays;

import com.gitlab.janecekpetr.benchmark.sorting.IntArraySorter;

public class InsertSort implements IntArraySorter {

	@Override
	public void sortInPlace(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int element = array[i];
			int targetIndex = Math.abs( Arrays.binarySearch(array, 0, i, element) + 1 );
            // Faster than System.arraycopy()!
            for (int j = i; j > targetIndex; j--) {
                array[j] = array[j-1];
            }
            array[targetIndex] = element;
        }
	}

	@Override
	public String toString() {
		return "InsertSort";
	}

}