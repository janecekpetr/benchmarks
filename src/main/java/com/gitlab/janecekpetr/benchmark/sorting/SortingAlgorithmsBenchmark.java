package com.gitlab.janecekpetr.benchmark.sorting;

import java.util.SplittableRandom;
import java.util.concurrent.TimeUnit;

import com.gitlab.janecekpetr.benchmark.sorting.SortingAlgorithmsBenchmark.SortingType;
import com.gitlab.janecekpetr.benchmark.sorting.strategy.BubbleSort;
import com.gitlab.janecekpetr.benchmark.sorting.strategy.HeapSort;
import com.gitlab.janecekpetr.benchmark.sorting.strategy.InsertSort;
import com.gitlab.janecekpetr.benchmark.sorting.strategy.JavaSort;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Fork(1)
@Warmup(iterations = 2, time = 3, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 5, timeUnit = TimeUnit.SECONDS)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
public class SortingAlgorithmsBenchmark {

	@Param({ "32", "10000", "100000" })
	private int arraySize;
	@Param
	private SortingType sortingType;

	private IntArraySorter sorter;
    private int[] array;

    @Setup
    public void setup() {
    	sorter = switch (sortingType) {
            case BUBBLE -> new BubbleSort();
            case HEAP -> new HeapSort();
            case INSERT -> new InsertSort();
            case JAVA -> new JavaSort();
        };
        array = new SplittableRandom(42)
			.ints(arraySize, 0, 1000)
			.toArray();
    }

	@Benchmark
    public int[] benchmark() {
        return sorter.sort(array);
	}

	public enum SortingType {
		BUBBLE, HEAP, INSERT, JAVA;
	}

}