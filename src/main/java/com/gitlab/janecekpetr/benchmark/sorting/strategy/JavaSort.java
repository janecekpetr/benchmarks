package com.gitlab.janecekpetr.benchmark.sorting.strategy;

import java.util.Arrays;

import com.gitlab.janecekpetr.benchmark.sorting.IntArraySorter;

public class JavaSort implements IntArraySorter {

	@Override
	public void sortInPlace(int[] array) {
		Arrays.sort(array);
	}

	@Override
	public String toString() {
		return "JavaSort";
	}

}