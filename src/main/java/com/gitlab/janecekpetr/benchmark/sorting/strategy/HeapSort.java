package com.gitlab.janecekpetr.benchmark.sorting.strategy;

import com.gitlab.janecekpetr.benchmark.sorting.IntArraySorter;

public class HeapSort implements IntArraySorter {

	private static final int HEAP_HEAD_INDEX = 0;

	@Override
	public void sortInPlace(int[] array) {
	  	maxHeapify(array);
	  	int size = array.length;
        while (size > 1) {
        	SortUtils.swap(array, HEAP_HEAD_INDEX, size - 1);
        	size--;
        	balanceNode(array, HEAP_HEAD_INDEX, size);
        }
	}

    private static void maxHeapify(int[] array) {
        int nonLeafs = array.length / 2;
        for (int i = nonLeafs; i >= HEAP_HEAD_INDEX; i--) {
    		balanceNode(array, i, array.length);
        }
    }

	private static void balanceNode(int[] array, int pos, int size) {
		while (pos >= HEAP_HEAD_INDEX) {
			pos = swapWithChildIfNeeded(array, pos, size);
		}
	}

	private static int swapWithChildIfNeeded(int[] array, int pos, int size) {
		final int leftChildPos = leftChildIndex(pos);

		// Special case: No children.
		if (leftChildPos >= size) {
			return -1;
		}

		final int value = array[pos];
		final int leftChildValue = array[leftChildPos];
		final int rightChildPos = rightChildIndex(pos);

		// Special case: One child only.
		if (rightChildPos == size) {
			return swapWithAConcreteChildIfNeeded(array, pos, value, leftChildPos, leftChildValue);
		}

		final int rightChildValue = array[rightChildPos];

		// Normal case: Two children. Swap with the bigger one if needed.
	    if (leftChildValue > rightChildValue) {
			return swapWithAConcreteChildIfNeeded(array, pos, value, leftChildPos, leftChildValue);
	    }
		return swapWithAConcreteChildIfNeeded(array, pos, value, rightChildPos, rightChildValue);
	}

	private static int leftChildIndex(int pos) {
		return (2*pos + 1);
	}

	private static int rightChildIndex(int pos) {
		return (2*pos + 2);
	}

	private static int swapWithAConcreteChildIfNeeded(int[] array, int pos, int value, int childPos, int childValue) {
		if (childValue > value) {
			SortUtils.swap(array, pos, childPos);
			return childPos;
		}
		return -1;
	}

	@Override
	public String toString() {
		return "HeapSort";
	}

}