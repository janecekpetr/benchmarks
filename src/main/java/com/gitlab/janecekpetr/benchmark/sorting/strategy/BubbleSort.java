package com.gitlab.janecekpetr.benchmark.sorting.strategy;

import com.gitlab.janecekpetr.benchmark.sorting.IntArraySorter;

public class BubbleSort implements IntArraySorter {

	@Override
	public void sortInPlace(int[] array) {
		for (int i = array.length - 1; i >= 0; i--) {
			for (int j = 0; j < i; j++) {
				if (array[j] > array[j + 1]) {
					SortUtils.swap(array, j, j+1);
				}
			}
		}
	}

	@Override
	public String toString() {
		return "BubbleSort";
	}

}