package com.gitlab.janecekpetr.benchmark.sorting.strategy;

class SortUtils {

	private SortUtils() {
		// a static utility class
	}

	public static void swap(int[] array, int pos1, int pos2) {
		int temp = array[pos1];
		array[pos1] = array[pos2];
		array[pos2] = temp;
	}

}
