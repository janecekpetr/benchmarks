package com.gitlab.janecekpetr.benchmark.sorting;

public interface IntArraySorter {

	public default int[] sort(int[] array) {
		int[] clone = array.clone();
		sortInPlace(clone);
		return clone;
	}

	public void sortInPlace(int[] array);

}