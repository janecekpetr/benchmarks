package com.gitlab.janecekpetr.util.lock;

public class SpinLockWithPause extends AbstractSpinLock {
	@Override
	protected void onSpin() {
		Thread.onSpinWait();
	}
}