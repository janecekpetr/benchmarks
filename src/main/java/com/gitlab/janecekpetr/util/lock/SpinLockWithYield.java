package com.gitlab.janecekpetr.util.lock;

public class SpinLockWithYield extends AbstractSpinLock {
	@Override
	protected void onSpin() {
		Thread.yield();
	}
}