package com.gitlab.janecekpetr.util.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

abstract class AbstractSpinLock implements Lock {

	private final AtomicBoolean locked = new AtomicBoolean(false);

	protected abstract void onSpin();

	@Override
	public void lock() {
		while (!tryLock()) {
			onSpin();
		}
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		if (!tryLock()) {
			return;
		}

		Thread currentThread = Thread.currentThread();
		while (!currentThread.isInterrupted() && !tryLock()) {
			onSpin();
		}

		if (Thread.interrupted()) {
			throw new InterruptedException();
		}
	}

	@Override
	public boolean tryLock() {
		return locked.compareAndSet(false, true);
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		if (tryLock()) {
			return true;
		}

		Thread currentThread = Thread.currentThread();
		long end = System.nanoTime() + unit.toNanos(time);
		while (!currentThread.isInterrupted() && (System.nanoTime() < end) && !tryLock()) {
			onSpin();
		}
		if (Thread.interrupted()) {
			throw new InterruptedException();
		}
		return locked.get();
	}

	@Override
	public void unlock() {
		locked.set(false);
	}

	@Override
	public Condition newCondition() {
		throw new UnsupportedOperationException("Not supported.");
	}
}