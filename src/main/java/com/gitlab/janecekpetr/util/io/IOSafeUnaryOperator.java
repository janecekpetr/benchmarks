package com.gitlab.janecekpetr.util.io;

import java.io.IOException;

@FunctionalInterface
public interface IOSafeUnaryOperator<T> {
    T apply(T t) throws IOException;
}