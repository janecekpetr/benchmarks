package com.gitlab.janecekpetr.util.io;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Unsynchronized, fixed-size {@link ByteArrayOutputStream} variant that optionally
 * shares its internal buffer for performance reasons.
 */
public class FixedSizeByteArrayOutputStream extends OutputStream {

    private final byte[] buffer;
    private int position = 0;

    public FixedSizeByteArrayOutputStream(int size) {
    	checkArgument(size >= 0, "Negative initial size: %s", size);
        this.buffer = new byte[size];
    }

    @Override
	public void write(int b) {
        buffer[position++] = (byte)b;
    }

    @Override
	public void write(byte[] source, int offset, int length) {
        System.arraycopy(source, offset, buffer, position, length);
        position += length;
    }

    public void writeBytes(byte[] source) {
        write(source, 0, source.length);
    }

    public void writeTo(OutputStream out) throws IOException {
        out.write(buffer, 0, position);
    }

    /**
     * Creates a newly allocated byte array. Its size is the current
     * size of this output stream and the valid contents of the buffer
     * have been copied into it.
     *
     * @return  the current contents of this output stream, as a byte array.
     * @see     java.io.ByteArrayOutputStream#size()
     */
    public byte[] toByteArray() {
        return Arrays.copyOf(buffer, position);
    }

    /** Look out! This shares the internal buffer without any additional checks. */
    public byte[] buffer() {
    	return buffer;
    }

    public int size() {
        return position;
    }

    @Override
	public String toString() {
        return Arrays.toString(buffer);
    }

    /**
     * Closing a {@code ByteArrayOutputStream} has no effect. The methods in
     * this class can be called after the stream has been closed without
     * generating an {@code IOException}.
     */
    @Override
	public void close() throws IOException {
    	// intentionally left empty
    }

}