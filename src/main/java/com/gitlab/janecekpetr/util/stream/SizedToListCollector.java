package com.gitlab.janecekpetr.util.stream;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class SizedToListCollector<T> implements Collector<T, List<T>, List<T>> {

	/** Somewhat arbitrary as it's only true for HotSpot and the value changed over time, but for all practical purposes as of OpenJDK 17... */
	private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
	private static final int SIZE_UNKNOWN = -1;
	private static final Set<Characteristics> IDENTITY_FINISH_SET = EnumSet.of(Characteristics.IDENTITY_FINISH);

	private final int size;

	public SizedToListCollector(long size) {
		if (size == Long.MAX_VALUE) {
			size = SIZE_UNKNOWN;
		} else if (size > MAX_ARRAY_SIZE) {
			throw new OutOfMemoryError("Required array length " + size + " is too large.");
		}
		this.size = (int)size;
	}

	@Override
	public Supplier<List<T>> supplier() {
		if (size == 0) {
			return List::of;
		}
		if (size == SIZE_UNKNOWN) {
			return ArrayList::new;
		}
		return () -> new ArrayList<>(size);
	}

	@Override
	public BiConsumer<List<T>, T> accumulator() {
		return List::add;
	}

	@Override
	public BinaryOperator<List<T>> combiner() {
		return (left, right) -> {
			left.addAll(right);
			return left;
		};
	}

	@Override
	public Function<List<T>, List<T>> finisher() {
		return Function.identity();
	}

	@Override
	public Set<Characteristics> characteristics() {
		return IDENTITY_FINISH_SET;
	}

}