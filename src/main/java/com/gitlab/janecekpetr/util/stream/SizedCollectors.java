package com.gitlab.janecekpetr.util.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SizedCollectors {

	/*
	 * It's an interesting debate whether to use spliterator.estimateSize() or spliterator.getExactSizeIfKnown(),
	 * or maybe even whether it's worth it to probe the characteristics of the Spliterator. The estimated size will
	 * e.g. overshoot in case of Stream#filter.
	 */

	private SizedCollectors() {
		// Static utility class.
	}

	public static <T> List<T> collectToSizedList(Stream<T> stream) {
		Spliterator<T> spliterator = stream.spliterator();
		return StreamSupport.stream(spliterator, false)
			.collect(toSizedList(spliterator.estimateSize()));
	}

	public static <T> List<T> collectToSizedListSequentially(Stream<T> stream) {
		Spliterator<T> spliterator = stream.spliterator();
		long size = spliterator.estimateSize();
		if (size == 0) {
			return List.of();
		}

		List<T> list;
		if (size == Long.MAX_VALUE) {
			// size unknown
			list = new ArrayList<>();
		} else if (size > Integer.MAX_VALUE - 8) {
			throw new OutOfMemoryError("Required array length " + size + " is too large.");
		} else {
			list = new ArrayList<>((int)size);
		}
		spliterator.forEachRemaining(list::add);
		return list;
	}

	public static <T> Collector<T, List<T>, List<T>> toSizedList(long size) {
		return new SizedToListCollector<>(size);
	}

}
