package com.gitlab.janecekpetr.util;

import java.awt.AWTException;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;

/** Moves the mouse a little every 59s and therefore prevents Screen sleep from kicking in. */
@State(Scope.Benchmark)
public class ScreenSleepDisablingState {

    private ScheduledExecutorService screenSleepDisabler;

    @Setup
    public void disableScreenSleep() {
        screenSleepDisabler = createScreenSleepDisabler();
    }

    @TearDown
    public void teardownScreenSleepDisabler() {
        if (screenSleepDisabler != null) {
            screenSleepDisabler.shutdown();
        }
    }

    private static ScheduledExecutorService createScreenSleepDisabler() {
        if (GraphicsEnvironment.isHeadless()) {
            return null;
        }

        Robot robot;
        try {
            robot = new Robot();
        } catch (AWTException _) {
            return null;
        }

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(runnable -> {
            Thread thread = new Thread(runnable, "screen-sleep-disabler");
            thread.setDaemon(true);
            return thread;
        });

        executorService.scheduleWithFixedDelay(() -> {
            Point loc = MouseInfo.getPointerInfo().getLocation();
            robot.mouseMove(loc.x + 1, loc.y);
            robot.mouseMove(loc.x, loc.y);
        }, 0, 59, TimeUnit.SECONDS);

        return executorService;
    }

}
